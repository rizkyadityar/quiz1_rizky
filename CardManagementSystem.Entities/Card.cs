﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CardManagementSystem.Entities
{
    public class Card
    {
        /// <summary>
        /// Id for card, primary key, auto increment (identity)
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CardId { get; set; }
        /// <summary>
        /// Name for card min 3 char, max 7 char
        /// </summary>
        [Required]
        public string CardName { get; set; }
        /// <summary>
        /// How much that card amount / count
        /// </summary>
        [Required]
        public int CardCount { get; set; }

    }
}
