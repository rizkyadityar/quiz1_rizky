﻿using CardManagementSystem.Entities;
using CardManagementSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardManagementSystem.Services
{
    public class CardService
    {
        private readonly ManagementDbContext _db;

        public CardService(ManagementDbContext managementDbContext)
        {
            this._db = managementDbContext;
        }

        /// <summary>
        /// For inserting card into database & table
        /// </summary>
        /// <param name="cardModel"></param>
        /// <returns>Boolean</returns>
        public async Task<bool> InsertCard(string name, int count)
        {
            this._db.Add(new Card
            {
                CardName = name,
                CardCount = count
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// to get how many data inside card table (not how many cards each card name has)
        /// </summary>
        /// <returns>card table data total count</returns>
        public int GetTotalData()
        {
            var totalCard = this._db
                .Cards
                .Count();

            return totalCard;
        }

        /// <summary>
        /// To get all cards in a list type, generally for view
        /// </summary>
        /// <returns>list of card</returns>
        public async Task<List<CardModel>> GetAllCardsAsync()
        {
            var cards = await this._db
                .Cards
                .Select(Q => new CardModel
                {
                    CardId = Q.CardId,
                    CardName = Q.CardName,
                    CardCount = Q.CardCount
                })
                .AsNoTracking()
                .ToListAsync();

            return cards;
        }
        /// <summary>
        /// getting cards for filtering and paging
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns>filtered/paged cards</returns>
        public async Task<List<CardModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            // set employee to queryable for  filter
            var query = this._db
                .Cards
                .AsQueryable();

            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.CardName.ToLower().Contains(filterByName.ToLower()));
            }

            // get data after filtering
            var cards = await query
                .Select(Q => new CardModel
                {
                    CardId = Q.CardId,
                    CardName = Q.CardName,
                    CardCount = Q.CardCount
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return cards;
        }

        /// <summary>
        /// get a specific card data based ln their id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>specific card data</returns>
        public async Task<CardModel> GetSpecificCardAsync(int? id)
        {
            var card = await this._db
                .Cards
                .Where(Q => Q.CardId == id)
                .Select(Q => new CardModel
                {
                    CardId = Q.CardId,
                    CardName = Q.CardName,
                    CardCount = Q.CardCount
                })
                .FirstOrDefaultAsync();

            return card;
        }

        /// <summary>
        /// for updating a specific card's data
        /// </summary>
        /// <param name="card"></param>
        /// <returns>boolean</returns>
        public async Task<bool> UpdateCardAsync(int id, string name, int count)
        {
            var cardModel = await this._db
                .Cards
                .Where(Q => Q.CardId == id)
                .FirstOrDefaultAsync();

            //only return false if CardId is false / doesn't exist
            if (cardModel == null)
            {
                return false;
            }

            //for updating the card's data
            cardModel.CardName = name;
            cardModel.CardCount = count;

            await this._db.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// For deleting a specific card
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns>boolean</returns>
        public async Task<bool> DeleteCardAsync(int cardId)
        {
            var cardModel = await this._db
                .Cards
                .Where(Q => Q.CardId == cardId)
                .FirstOrDefaultAsync();

            //if the card wasn't found / false input id
            if (cardModel == null)
            {
                return false;
            }

            this._db.Remove(cardModel);
            await this._db.SaveChangesAsync();

            return true;
        }
    }
}
