﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CardManagementSystem.Models
{
    public class CardModel
    {
        /// <summary>
        /// Id is the primary key & auto increment
        /// </summary>
        public int CardId { get; set; }
        /// <summary>
        /// for card name
        /// </summary>
        [MinLength(3),MaxLength(7)]
        [Required]
        public string CardName { get; set; }
        /// <summary>
        /// how many card each card name have
        /// </summary>
        public int CardCount { get; set; }
    }
}
