﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardManagementSystem.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// for API to send back a message
        /// </summary>
        public string ResponseMessage { set; get; }
    }
}
