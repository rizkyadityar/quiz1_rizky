﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardManagementSystem.Models;
using CardManagementSystem.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardManagementSystem.API
{
    [Route("api/v1/card")]
    [ApiController]
    public class CardApiController : ControllerBase
    {
        private readonly CardService _cardServiceMan;

        public CardApiController(CardService cardService )
        {
            this._cardServiceMan = cardService;
        }

        /// <summary>
        /// to get all cards on table card
        /// </summary>
        /// <returns>list of cards</returns>
        [HttpGet("all-card", Name = "getAllCard")]
        public async Task<ActionResult<List<CardModel>>> GetAllCardsAsync()
        {
            var cards = await _cardServiceMan.GetAllCardsAsync();
            return Ok(cards);
        }

        /// <summary>
        /// for inserting a new card into card table
        /// </summary>
        /// <param name="value"></param>
        /// <returns>HTTP response</returns>
        [HttpPost("insert", Name = "insertCard")]
        public async Task<ActionResult<ResponseModel>> InsertNewCardAsync([FromQuery]string name, int count)
        {
            var isSuccess = await _cardServiceMan.InsertCard(name, count);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data."
            });
        }

        /// <summary>
        /// to get a specific card's data
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns>card's data</returns>
        [HttpGet("specific-card", Name = "getSpesificCard")]
        public async Task<ActionResult<CardModel>> GetSpecificCardAsync(string cardId)
        {
            if (string.IsNullOrEmpty(cardId) == true)
            {
                return BadRequest(null);
            }

            var id = int.Parse(cardId);
            var card = await _cardServiceMan.GetSpecificCardAsync(id);
            if (card == null)
            {
                return BadRequest(null);
            }
            return Ok(card);
        }

        /// <summary>
        /// for changing a specific card data's
        /// </summary>
        /// <param name="value"></param>
        /// <returns>http response</returns>
        [HttpPut("update", Name = "updateCard")]
        public async Task<ActionResult<ResponseModel>> UpdateCardAsync([FromQuery]int id, string name, int count)
        {
            var isSuccess = await _cardServiceMan.UpdateCardAsync(id, name, count);

            //false if card id is false
            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success update data"
            });
        }

        /// <summary>
        /// for deleting a specific card by their id
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Http response</returns>
        [HttpDelete("delete", Name = "deleteCard")]
        public async Task<ActionResult<ResponseModel>> DeleteCardAsync(CardModel value)
        {
            var isSuccess = await _cardServiceMan.DeleteCardAsync(value.CardId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete card {value.CardName}"
            });
        }

        /// <summary>
        /// Api for filter by name
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        [HttpGet("filter-data", Name ="getFilterData")]
        public async Task<ActionResult<List<CardModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _cardServiceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }


        /// <summary>
        /// Api for getting the total data on the table
        /// </summary>
        /// <returns></returns>
        [HttpGet("total-data", Name ="getTotalData")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = _cardServiceMan.GetTotalData();

            return Ok(totalData);
        }
    }
}
